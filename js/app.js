const carrito = document.querySelector('#carrito');
const listaCursos = document.querySelector('#lista-cursos');
const contenedorCarrito = document.querySelector('#lista-carrito tbody');
const vaciarCarrito = document.querySelector('#vaciar-carrito');

let articulosCarrito = [];

cargarEventListeners();
// Funcion que contiene los listeners
function cargarEventListeners () {
  // Evento click para agregar curso a carrito
  listaCursos.addEventListener('click', agregarCurso);
  // Evento click para eliminar curso del carrito
  carrito.addEventListener('click', eliminarCurso);
  // Evento click para vaciar carrito
  vaciarCarrito.addEventListener('click', () => {
    articulosCarrito = [];
    limpiarHTML();
  });
}

// Funciones
function agregarCurso(e) {
  // Prevenimos que la pagina se valla hacia arriba
  e.preventDefault();
  // Solo activamos cuando se presione en el boton agregar-carrito
  if (e.target.classList.contains('agregar-carrito')) {
    // Seleccionamos el elemento Padre => Padre => boton agregar-carrito
    const cursoSeleccionado = e.target.parentElement.parentElement;
    leerDatosCurso(cursoSeleccionado);
  }
}

function leerDatosCurso(curso) {
  const nuevoCurso = {
    id: curso.querySelector('a').getAttribute('data-id'),
    imagen: curso.querySelector('img').src,
    titulo: curso.querySelector('h4').textContent,
    precio: curso.querySelector('.precio span').textContent,
    cantidad: 1
  }
  // Revisa si el elemento ya existe en el carrito
  const existe = articulosCarrito.some(curso => curso.id === nuevoCurso.id);

  if (existe) {
    // Actualizamos cantidad
    const cursos = articulosCarrito.map(curso => {
      if (curso.id === nuevoCurso.id) {
        curso.cantidad++;
      }
      return curso;
    });
    articulosCarrito = [...cursos];
  } else {
    // Agrega elementos al arreglo carrito
    articulosCarrito = [...articulosCarrito, nuevoCurso];
  }
  carritoHTML();
}

// Muestra el ecarrito en el DOM
function carritoHTML() {
  //Limpiar html
  limpiarHTML();
  // Recorre arreglo de articulos y los crea
  articulosCarrito.forEach( curso => {
    const { imagen, titulo, precio, cantidad, id } = curso;
    const row = document.createElement('tr');
    row.innerHTML = `
      <td><img src="${imagen}" width="100"></td>
      <td>${titulo}</td>
      <td>${precio}</td>
      <td>${cantidad}</td>
      <td>
        <a href="#" class="borrar-curso" data-id="${id}" >X</a>
      </td>
    `;
    // Agrega el html del carrito en el tbody
    contenedorCarrito.appendChild(row);
  });
}
// Limpia el carrito HTML
function limpiarHTML() {
  // Forma lenta
  // contenedorCarrito.innerHTML = '';
  while (contenedorCarrito.firstChild) {
    contenedorCarrito.removeChild(contenedorCarrito.firstChild);
  }
}

function eliminarCurso(e) {
  if (e.target.classList.contains('borrar-curso')) {
    const id = e.target.getAttribute('data-id');
    articulosCarrito = articulosCarrito.filter(curso => curso.id !== id);
    carritoHTML();
  }
}